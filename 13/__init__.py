from pathlib import Path

from . import day


if __name__ == "__main__":
    input_path = Path(__file__).parent
    with open(input_path / "input.txt") as f:
        data = f.read().splitlines()

    arrival_time = int(data[0])
    buses = data[1]

    print(f"solution1: {day.solution1(arrival_time, buses)}")
    print(f"solution2: {day.solution2(buses)}")
