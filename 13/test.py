import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        arrival_time = 939
        buses = "7,13,x,x,59,x,31,19"

        expected = 295
        self.assertEqual(expected, day.solution1(arrival_time, buses))

    def test2(self):
        buses = "7,13,x,x,59,x,31,19"

        expected = 1068781
        self.assertEqual(expected, day.solution2(buses))
