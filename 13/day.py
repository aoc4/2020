from typing import List

from math import prod


def solution1(arrival_time: int, buses_line: str) -> int:
    buses = [int(bus) for bus in buses_line.split(",") if bus != "x"]
    times_to_wait = [bus - arrival_time % bus for bus in buses]
    least_to_wait = min(times_to_wait)
    least_to_wait_bus_num = buses[times_to_wait.index(least_to_wait)]
    return least_to_wait * least_to_wait_bus_num


def chinese_remainder(numbers: List[int], remainders: List[int]) -> int:
    product = prod(numbers)
    total = sum(r * (product // n) * pow(product // n, -1, n) for r, n in zip(remainders, numbers))
    return total % product


def solution2(buses_lines: str) -> int:
    buses_and_offsets = [(idx, int(bus)) for idx, bus in enumerate(buses_lines.split(",")) if bus != "x"]
    remainders = [bus - offset for offset, bus in buses_and_offsets]
    buses = [bus for _, bus in buses_and_offsets]
    return chinese_remainder(buses, remainders)
