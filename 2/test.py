import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        data = """1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc"""
        expected = 2
        self.assertEqual(expected, day.solution1(data))
