from pathlib import Path
from typing import Callable

from . import day


input_path = Path(__file__).parent


def get_solution(filename: str, solution_func: Callable[[str], int]) -> int:
    with open(input_path / filename) as f:
        data = f.read()

    return solution_func(data)


if __name__ == "__main__":
    print(f"solution1: {get_solution('input.txt', day.solution1)}")
    # print(f"solution1 big1: {get_solution('big.1.txt', day.solution1)}") # 134116
    # print(f"solution1 big2: {get_solution('big.2.txt', day.solution1)}") # 17862
    print(f"solution2: {get_solution('input.txt', day.solution2)}")
    # print(f"solution2 big1: {get_solution('big.1.txt', day.solution2)}")
    # print(f"solution2 big2: {get_solution('big.2.txt', day.solution2)}")