import re
from typing import Tuple


def parse_line(line: str) -> Tuple[int, int, str, str]:
    """ lower, upper, character, password """
    match = re.match(r"(\d+)-(\d+) (\S): (\S+)", line)
    return int(match.group(1)), int(match.group(2)), match.group(3), match.group(4)


def is_valid1(line: str) -> bool:
    lower_lim, upper_lim, char, psw = parse_line(line)
    return lower_lim <= psw.count(char) <= upper_lim


def solution1(data: str) -> int:
    return sum(is_valid1(line) for line in data.splitlines())


def is_valid2(line: str) -> bool:
    first_idx, second_idx, char, psw = parse_line(line)

    if first_idx > len(psw) or second_idx > len(psw):  # arbitrary check for big files
        return False

    first_match = psw[first_idx - 1] == char
    second_match = psw[second_idx - 1] == char
    return first_match != second_match  # XOR


def solution2(data: str) -> int:
    return sum(is_valid2(line) for line in data.splitlines())