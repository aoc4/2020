from typing import List, Tuple


def parse_instructions(data: str) -> List[Tuple[str, int]]:
    return [(line[0], int(line[1:])) for line in data.splitlines()]


def cardinal_go(x: int, y: int, instruction: str, num: int) -> Tuple[int, int]:
    if instruction == "N":
        y += num
    elif instruction == "S":
        y -= num
    elif instruction == "E":
        x += num
    elif instruction == "W":
        x -= num
    return x, y
    

def solution1(data: str) -> int:
    instructions: List[Tuple[str, int]] = parse_instructions(data)
    direction = "E"
    directions = "NESW"
    x = y = 0

    for instruction, num in instructions:
        if instruction in directions:
            x, y = cardinal_go(x, y, instruction, num)
        elif instruction in "LR":
            curr_idx = directions.index(direction)
            new_dir_idx = (curr_idx + (num if instruction == "R" else -num) // 90) % 4
            direction = directions[new_dir_idx]
        elif instruction == "F":
            x, y = cardinal_go(x, y, direction, num)

    return abs(x) + abs(y)


def solution2(data: str) -> int:
    instructions: List[Tuple[str, int]] = parse_instructions(data)
    directions = "NESW"
    w_x = 10  # waypoint
    w_y = 1
    s_x = 0  # ship
    s_y = 0

    for instruction, num in instructions:
        if instruction in directions:
            w_x, w_y = cardinal_go(w_x, w_y, instruction, num)
        elif instruction in "LR":
            # hurr durr, couldn't figure out one off turning
            for _ in range(num // 90):
                if instruction == "R":
                    w_x, w_y = w_y, -w_x
                else:
                    w_x, w_y = -w_y, w_x
            
        elif instruction == "F":
            s_x += num * w_x
            s_y += num * w_y

    return abs(s_x) + abs(s_y)
