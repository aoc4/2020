import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        data = """F10
N3
F7
R90
F11"""

        expected = 25
        self.assertEqual(expected, day.solution1(data))

    def test2(self):
        data = """F10
N3
F7
R90
F11"""

        expected = 286
        self.assertEqual(expected, day.solution2(data))
