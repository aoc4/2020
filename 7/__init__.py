from pathlib import Path

from . import day


if __name__ == "__main__":
    input_path = Path(__file__).parent
    with open(input_path / "input.txt") as f:
        data = f.read()

    print(f"solution1: {day.solution1(data, 'shiny gold')}")
    print(f"solution1: {day.solution2(data, 'shiny gold')}")