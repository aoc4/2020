from typing import Dict

from functools import lru_cache

import re


bags = {}


def parse_data(data: str) -> Dict[str, Dict[str, int]]:
    sub_bag_regex = r"(\d+) (\S+ \S+) bags?"

    lines = data.splitlines()
    for line in lines:
        main_bag, sub_bags = line.split("bags contain")
        bags[main_bag.strip()] = {}
        if "no other" in sub_bags:
            continue
        for sub_bag in sub_bags.split(","):
            match = re.search(sub_bag_regex, sub_bag)
            bags[main_bag.strip()][match.group(2)] = int(match.group(1))
    return bags


@lru_cache(maxsize=None)
def can_contain(my_bag: str, bag: str) -> bool:
    sub_bags = bags[bag]
    for sub_bag in sub_bags:
        if sub_bag == my_bag:
            return True

        if can_contain(my_bag, sub_bag):
            return True

    return False


def solution1(data: str, my_bag: str) -> int:
    bags: Dict[str, Dict[str, int]] = parse_data(data)

    return sum(can_contain(my_bag, bag) for bag in bags)


@lru_cache(maxsize=None)
def contains_bags(my_bag: str) -> int:
    sub_bags = bags[my_bag]
    bag_amount = 1  # self
    for sub_bag, sub_bag_count in sub_bags.items():
        bag_amount += sub_bag_count * contains_bags(sub_bag)

    return bag_amount


def solution2(data: str, my_bag: str) -> int:
    bags: Dict[str, Dict[str, int]] = parse_data(data)

    return contains_bags(my_bag) - 1  # - self