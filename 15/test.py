import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        data = """0,3,6"""

        expected = 436
        self.assertEqual(expected, day.solution1(data, 2020))

    def test2(self):
        data = """1,3,2"""

        expected = 1
        self.assertEqual(expected, day.solution1(data))

    def test3(self):
        data = """2,1,3"""

        expected = 10
        self.assertEqual(expected, day.solution1(data))

    def test4(self):
        data = """1,2,3"""

        expected = 27
        self.assertEqual(expected, day.solution1(data))

    def test5(self):
        data = """2,3,1"""

        expected = 78
        self.assertEqual(expected, day.solution1(data))

    def test6(self):
        data = """3,2,1"""

        expected = 438
        self.assertEqual(expected, day.solution1(data))

    def test7(self):
        data = """3,1,2"""

        expected = 1836
        self.assertEqual(expected, day.solution1(data))

    def test8(self):
        data = """0,3,6"""

        expected = 6
        self.assertEqual(expected, day.solution1(data, 3))

    def test9(self):
        data = """0,3,6"""

        expected = 0
        self.assertEqual(expected, day.solution1(data, 4))

    def test10(self):
        data = """0,3,6"""

        expected = 3
        self.assertEqual(expected, day.solution1(data, 5))

    def test11(self):
        data = """0,3,6"""

        expected = 3
        self.assertEqual(expected, day.solution1(data, 6))
