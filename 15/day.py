def solution1(data: str, end: int = 2020) -> int:
    memory = {int(numb): time + 1 for time, numb in enumerate(data.split(','))}
    numb = list(memory.keys())[-1]
    for i in range(len(memory), end):
        # add last index to that number
        memory[numb], numb = (i, i - memory[numb]) if numb in memory else (i, 0)

    return numb
