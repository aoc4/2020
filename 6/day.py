from collections import Counter


def solution1(data: str) -> int:
    groups = data.split("\n\n")
    ans_sum = 0

    for group in groups:
        answers = set()
        for line in group.splitlines():
            answers.update(line)
        ans_sum += len(answers)

    return ans_sum

def solution2(data: str) -> int:
    groups = data.split("\n\n")
    ans_sum = 0

    for group in groups:
        answers = Counter()
        participants = group.splitlines()

        for line in participants:
            answers.update(line)

        for _, count in answers.most_common():
            if count == len(participants):
                ans_sum += 1

    return ans_sum