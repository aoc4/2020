import unittest

from . import day


class DayTest(unittest.TestCase):
    def test_p1_1(self):
        data = "abc"

        expected = 3
        self.assertEqual(expected, day.solution1(data))

    def test_p1_2(self):
        data = """a
b
c"""

        expected = 3
        self.assertEqual(expected, day.solution1(data))

    def test_p1_3(self):
        data = """ab
ac"""

        expected = 3
        self.assertEqual(expected, day.solution1(data))

    def test_p1_4(self):
        data = """a
a
a
a"""
        expected = 1
        self.assertEqual(expected, day.solution1(data))

    def test_p1_5(self):
        data = """b"""
        expected = 1
        self.assertEqual(expected, day.solution1(data))

    def test_p1_6(self):
        data = """abc

a
b
c

ab
ac

a
a
a
a

b"""
        expected = 11
        self.assertEqual(expected, day.solution1(data))

    def test_p2_1(self):
        data = "abc"

        expected = 3
        self.assertEqual(expected, day.solution1(data))

    def test_p2_2(self):
        data = """a
b
c"""

        expected = 0
        self.assertEqual(expected, day.solution2(data))

    def test_p2_3(self):
        data = """ab
ac"""

        expected = 1
        self.assertEqual(expected, day.solution2(data))

    def test_p2_4(self):
        data = """a
a
a
a"""
        expected = 1
        self.assertEqual(expected, day.solution2(data))

    def test_p2_5(self):
        data = """b"""
        expected = 1
        self.assertEqual(expected, day.solution2(data))

    def test_p2_6(self):
        data = """abc

a
b
c

ab
ac

a
a
a
a

b"""
        expected = 6
        self.assertEqual(expected, day.solution2(data))
