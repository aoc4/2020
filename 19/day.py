from typing import Dict, List, Set

from itertools import product


def get_dependencies(line: str) -> List[int]:
    tokens = line.split()
    return [int(token) for token in tokens if token.isdigit()]


def build_rule(line: str, rules: Dict[int, List[str]]) -> List[str]:
    tokens = line.split()
    
    if '"' in line:  # static value
        return [tokens[0][1:-1]]

    values = []
    tmp = []
    for token in tokens:
        if token == "|":
            values.extend(tmp)
            tmp = []
        else:
            idx = int(token)
            if tmp:
                tmp = [a + b for a, b in product(tmp, rules[idx])]
            else:
                tmp = rules[idx]

    values.extend(tmp)
    return values


def get_needed_deps(lines: List[str]) -> Set[int]:
    deps = {}
    for line in lines:
        idx, d = line.split(":")
        idx = int(idx)
        deps[idx] = get_dependencies(d)

    buffer = deps[0]
    needed = list(buffer)
    while buffer:
        tmp_needed = []
        for dep in buffer:
            tmp_needed.extend(deps[dep])
        needed.extend(tmp_needed)
        buffer = list(tmp_needed)
        
    return set(needed + [0])

def parse_rules(rules_str: str) -> Dict[int, List[str]]:
    # produce a bunch of rules
    rules_lines = rules_str.splitlines()
    rules = {}
    needed = get_needed_deps(rules_lines)
    while len(rules_lines) != len(rules):
        for r in rules_lines:
            idx, line = r.split(":")
            idx = int(idx)
            if idx in rules or idx not in needed:
                continue
            dependencies = get_dependencies(line)
            deps_met = all(dep in rules for dep in dependencies)
            if deps_met:
                rules[idx] = build_rule(line, rules)

    return rules


def is_valid(msg: str, rule: List[str]) -> bool:
    for r in rule:
        if r == msg:
            return True
    return False


def solution1(data: str) -> int:
    rules_str, msg_str = data.split("\n\n")
    rules = parse_rules(rules_str)
    messages = msg_str.splitlines()

    return sum(is_valid(msg, rules[0]) for msg in messages)


def solution2(data: str) -> int:
    rules_str, msg_str = data.split("\n\n")
    rules_str = rules_str.replace("8: 42", "8: 42 | 42 8").replace("11: 42 31", "11: 42 31 | 42 11 31")
    rules = parse_rules(rules_str)
    
    messages = msg_str.splitlines()

    return sum(is_valid(msg, rules[0]) for msg in messages)