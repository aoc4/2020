import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        data = """1721
979
366
299
675
1456"""
        expected = 514579
        self.assertEqual(expected, day.solution1(data))

    def test2(self):
        data = """1721
979
366
299
675
1456"""
        expected = 241861950
        self.assertEqual(expected, day.solution2(data))