from typing import List, Tuple


def get_sum_pair(numbers: List[int], sum_target: int) -> Tuple[int, int]:
    left = 0
    right = len(numbers) - 1
    while numbers[left] + numbers[right] != sum_target and left < right:
        if numbers[left] + numbers[right] > sum_target:
            right -= 1
        else:
            left += 1

    if left == right:
        raise Exception

    return numbers[left], numbers[right]


def solution1(data: str, sum_target: int = 2020) -> int:
    numbers = sorted(int(line) for line in data.splitlines())
    a, b = get_sum_pair(numbers, sum_target)
    return a * b


def solution2(data: str, sum_target: int = 2020) -> int:
    numbers = sorted(int(line) for line in data.splitlines())
    a = b = right = 0
    for idx in range(1, len(numbers)):
        right = numbers[-idx]
        sub_numbers = numbers[:-idx]
        try:
            a, b = get_sum_pair(sub_numbers, sum_target - right)
            break
        except Exception:
            pass

    return a * b * right
