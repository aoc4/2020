from typing import Callable, Dict, List

from string import hexdigits

fields = {
    "byr": lambda x: 1920 <= int(x) <= 2002,
    "iyr": lambda x: 2010 <= int(x) <= 2020,
    "eyr": lambda x: 2020 <= int(x) <= 2030,
    "hgt": lambda x: 59 <= int(x[:-2]) <= 76 if x.endswith("in") else 150 <= int(x[:-2]) <= 193,
    "hcl": lambda x: x[0] == '#' and len(x[1:]) == 6 and all(c in hexdigits for c in x[1:]),
    "ecl": lambda x: x in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"],
    "pid": lambda x: len(x) == 9 and x.isdigit(),
    "cid": lambda x: x
}

optional = ["cid"]

def parse_passports(data: str) -> List[Dict[str, str]]:
    # separated by blank lines
    documents: List[str] = data.split("\n\n")
    return [{pair.split(":")[0]: pair.split(":")[1] for pair in document.split()} for document in documents]


def has_fields_present(passport: Dict[str, str], fields: List[str], optional: List[str]) -> bool:
    passport_key_set = set(passport.keys())
    differences = passport_key_set.symmetric_difference(set(fields))
    return len(differences - set(optional)) == 0


def solution1(data: str) -> int:
    passports: List[Dict[str, str]] = parse_passports(data)
    return sum(has_fields_present(passport, list(fields.keys()), optional) for passport in passports)


def is_valid(passport: Dict[str, str], fields: Dict[str, Callable[[str], bool]], optional: List[str]):
    if not has_fields_present(passport, list(fields.keys()), optional):
        return False

    return all(fields[key](value) for key, value in passport.items())


def solution2(data: str) -> int:
    passports: List[Dict[str, str]] = parse_passports(data)
    return sum(is_valid(passport, fields, optional) for passport in passports)
