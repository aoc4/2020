from typing import List


def has_pair_sum(numbers: List[int], sum_target: int) -> bool:
    left = 0
    right = len(numbers) - 1
    while numbers[left] + numbers[right] != sum_target and left < right:
        if numbers[left] + numbers[right] > sum_target:
            right -= 1
        else:
            left += 1

    if left == right:
        return False

    return True


def solution1(data: str, prev_numbers: int = 25) -> int:
    numbers = [int(line) for line in data.splitlines()]
    for idx in range(len(numbers) - prev_numbers - 1):
        preamble = sorted(numbers[idx: idx + prev_numbers])
        num = numbers[idx + prev_numbers]
        if not has_pair_sum(preamble, num):
            return num
    return -1


def find_sum_to_target(numbers: List[int], target: int, nums: List[int] = []) -> List[int]:
    right = numbers.index(target)

    for left in range(right - 1):
        candidate = []
        for idx in range(left, right):
            candidate.append(numbers[idx])
            total = sum(candidate)
            if total == target:
                return candidate
            elif total > target:
                break


def solution2(data: str, weak_target: int) -> int:
    numbers = [int(line) for line in data.splitlines()]
    sum_parts = sorted(find_sum_to_target(numbers, weak_target))
    return sum_parts[0] + sum_parts[-1]
