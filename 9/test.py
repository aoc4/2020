import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        data = """35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576"""

        expected = 127
        self.assertEqual(expected, day.solution1(data, prev_numbers=5))

    def test2(self):
        data = """35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576"""

        expected = 62
        self.assertEqual(expected, day.solution2(data, weak_target=127))