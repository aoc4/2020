from pathlib import Path

from . import day


if __name__ == "__main__":
    input_path = Path(__file__).parent
    with open(input_path / "input.txt") as f:
        data = f.read()

    ans1 = day.solution1(data)
    print(f"solution1: {ans1}")
    print(f"solution2: {day.solution2(data, weak_target=ans1)}")