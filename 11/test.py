import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        data = """L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL"""

        expected = 37
        self.assertEqual(expected, day.solution1(data))

    def test2(self):
        data = """L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL"""

        expected = 26
        self.assertEqual(expected, day.solution2(data))
