from typing import List


def get_adjacent_seated(cx: int, cy: int, seats: List[List[str]]):
    x_min = max(0, cx - 1)
    x_max = min(len(seats), cx + 2)
    y_min = max(0, cy - 1)
    y_max = min(len(seats[0]), cy + 2)

    return sum(sum(seats[x][y] == "#" for y in range(y_min, y_max)) for x in range(x_min, x_max))


def update_seats(seats: List[List[str]]) -> List[List[str]]:
    new_seats: List[List[str]] = [row[:] for row in seats]  # copy

    for row_idx in range(len(seats)):
        for col_idx in range(len(seats[row_idx])):
            value = seats[row_idx][col_idx]
            if value == ".":
                continue  # ground

            seated = get_adjacent_seated(row_idx, col_idx, seats)

            if value == "#":
                new_seats[row_idx][col_idx] = "L" if seated >= 5 else "#"
            elif value == "L":
                new_seats[row_idx][col_idx] = "#" if seated == 0 else "L"

    return new_seats


def get_seated(plan: List[List[str]]):
    return sum(row.count("#") for row in plan)


def solution1(data: str) -> int:
    plan: List[List[str]] = [[char for char in line] for line in data.splitlines()]
    seated = -1

    while get_seated(plan) != seated:
        seated = get_seated(plan)
        plan = update_seats(plan)

    return seated


def is_closest_seated(items: List[str]) -> int:
    for item in items:
        if item == ".":
            continue
        return item == "#"

    return False  # no seats in that direction


def get_closest_seated(cx: int, cy: int, seats: List[List[str]]):
    # honestly just go in every direction and find first non ground spot
    lines = [
        # west
        seats[cx][:cy][::-1],  # reverse and find first non ".""
        # nw
        [seats[x][y] for x, y in zip(range(cx - 1, -1, -1), range(cy - 1, -1, -1))],
        # north
        [seats[x][cy] for x in range(cx)][::-1],
        # ne
        [seats[x][y] for x, y in zip(range(cx)[::-1], range(cy + 1, len(seats[0])))],
        # east
        seats[cx][cy + 1:],
        # se
        [seats[x][y] for x, y in zip(range(cx + 1, len(seats)), range(cy + 1, len(seats[0])))],
        # south
        [seats[x][cy] for x in range(cx + 1, len(seats))],
        # sw
        [seats[x][y] for x, y in zip(range(cx + 1, len(seats)), range(cy)[::-1])]
    ]

    return sum(is_closest_seated(line) for line in lines)


def update_seats2(seats: List[List[str]]) -> List[List[str]]:
    new_seats: List[List[str]] = [row[:] for row in seats]  # copy

    for row_idx in range(len(seats)):
        for col_idx in range(len(seats[row_idx])):
            value = seats[row_idx][col_idx]

            if value == ".":
                continue

            seated = get_closest_seated(row_idx, col_idx, seats)

            if value == "#":
                new_seats[row_idx][col_idx] = "L" if seated >= 5 else "#"
            elif value == "L":
                new_seats[row_idx][col_idx] = "#" if seated == 0 else "L"

    return new_seats


def solution2(data: str) -> int:
    plan: List[List[str]] = [[char for char in line] for line in data.splitlines()]
    seated = -1

    while get_seated(plan) != seated:
        seated = get_seated(plan)
        plan = update_seats2(plan)

    return seated
