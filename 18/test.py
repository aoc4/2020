import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        data = "2 * 3 + (4 * 5)"

        expected = 26
        self.assertEqual(expected, day.solution1(data))

    def test2(self):
        data = "5 + (8 * 3 + 9 + 3 * 4 * 3)"

        expected = 437
        self.assertEqual(expected, day.solution1(data))

    def test3(self):
        data = "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"

        expected = 12240
        self.assertEqual(expected, day.solution1(data))

    def test4(self):
        data = "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"

        expected = 13632
        self.assertEqual(expected, day.solution1(data))

    def test5(self):
        data = "1 + 2 * 3 + 4 * 5 + 6"

        expected = 71
        self.assertEqual(expected, day.solution1(data))

    def test6(self):
        data = "1 + (2 * 3) + (4 * (5 + 6))"

        expected = 51
        self.assertEqual(expected, day.solution1(data))

    def test21(self):
        data = "2 * 3 + (4 * 5)"

        expected = 46
        self.assertEqual(expected, day.solution2(data))

    def test22(self):
        data = "5 + (8 * 3 + 9 + 3 * 4 * 3)"

        expected = 1445
        self.assertEqual(expected, day.solution2(data))

    def test23(self):
        data = "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"

        expected = 669060
        self.assertEqual(expected, day.solution2(data))

    def test24(self):
        data = "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"

        expected = 23340
        self.assertEqual(expected, day.solution2(data))

    def test25(self):
        data = "1 + 2 * 3 + 4 * 5 + 6"

        expected = 231
        self.assertEqual(expected, day.solution2(data))

    def test26(self):
        data = "1 + (2 * 3) + (4 * (5 + 6))"

        expected = 51
        self.assertEqual(expected, day.solution2(data))

    def test27(self):
        data = "1 + 2 + 3 + 4 + 5"

        expected = 15
        self.assertEqual(expected, day.solution2(data))