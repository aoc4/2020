from typing import Any, Callable, Dict, List

import json
import re


ACTIONS: Dict[str, Callable[[int, int], int]] = {
    "+": lambda a, b: a + b,
    "*": lambda a, b: a * b
}

MAPPINGS_TO_JSON = {
    "(": "[",
    ")": "]",
    "+": '"+"',
    "*": '"*"',
    " ": ","
}


def get_result(instruction: List[Any]) -> int:
    while len(instruction) != 1:
        left, action, right, *rest = instruction

        if not isinstance(left, int):
            left = get_result(left)

        if not isinstance(right, int):
            right = get_result(right)

        res = ACTIONS[action](left, right)
        instruction = [res] + rest

    if isinstance(instruction[0], list):
        return get_result(instruction[0])

    return instruction[0]


def solution1(data: str) -> int:
    # super cool approach by
    # https://www.reddit.com/r/adventofcode/comments/kfeldk/2020_day_18_solutions/gg9xhka
    instructions = []

    for line in data.splitlines():
        for search, replace in MAPPINGS_TO_JSON.items():
            line = line.replace(search, replace)
        instructions.append(json.loads(f"[{line}]"))

    return sum(get_result(instruction) for instruction in instructions)


def get_left_parenthesis_index(index: int, instruction: str) -> int:
    left = instruction[:index]
    balance = 0
    for idx in range(len(left) - 1, -1, -1):
        if left[idx] == " ":
            continue  # ignore whitespaces
        elif left[idx] == ")":
            balance += 1
        elif left[idx] == "(":
            balance -= 1
        
        if balance <= 0:
            return idx
        
    return 0  # failsafe to just wrap from start


def get_right_parenthesis_index(index: int, instruction: str) -> int:
    right = instruction[index + 1:]
    balance = 0
    for idx in range(len(right)):
        if right[idx] == " ":
            continue  # ignore whitespace
        elif right[idx] == ")":
            balance -= 1
        elif right[idx] == "(":
            balance += 1

        if balance <= 0:
            return index + idx

    return len(instruction)  # failsafe


def remove_chained_addition_bug(instruction: str) -> str:
    return re.sub(r"\((\d+)\)", r"\1", instruction)


def parenthesize_addition(instruction: str) -> str:
    """Parentheses for additions to simulate priority"""
    indices = (idx for idx, char in enumerate(instruction) if char == "+")
    inserts = []
    for index in indices:
        left_index = get_left_parenthesis_index(index, instruction)
        right_index = get_right_parenthesis_index(index, instruction)
        inserts.append((left_index, "("))
        inserts.append((right_index + 1, ")"))

    inserts.sort(key=lambda x: x[0], reverse=True)

    for idx, char in inserts:  # from right to left
        if char == "(":
            instruction = instruction[:idx] + char + instruction[idx:]
        elif char == ")":
            instruction = instruction[:idx + 1] + char + instruction[idx + 1:]

    return remove_chained_addition_bug(instruction)


def solution2(data: str) -> int:
    instructions = []

    for line in data.splitlines():
        # to prioritize sum, just add clauses
        line = parenthesize_addition(line)
        for search, replace in MAPPINGS_TO_JSON.items():
            line = line.replace(search, replace)
        instructions.append(json.loads(f"[{line}]"))

    return sum(get_result(instruction) for instruction in instructions)
