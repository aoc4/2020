"""Day 20 solution(s)"""

from typing import Dict, List

from math import prod
import re


def parse_tiles(data: str) -> Dict[int, List[str]]:
    """Parse data into identified tiles

    Arguments:
        data (str): raw data, see input.txt for example

    Returns:
        Dict[int, List[str]]: mapping of tile id to binary matrix representation

    """
    tiles = {}
    batches = data.split("\n\n")
    for batch in batches:
        lines = batch.splitlines()
        match = re.match(r"Tile (\d+):", lines[0])
        tile_id = int(match.group(1))
        tiles[tile_id] = lines[1:]
    return tiles


def get_edges(tile: List[str]) -> List[str]:
    """ Retrieve tile's edges

    Arguments:
        tile (List[str]): tile data

    Returns:
        List[str]: List of edges

    """
    return [
        tile[0],  # top
        tile[-1],  # bottom
        "".join(row[0] for row in tile),  # left
        "".join(row[-1] for row in tile),  # right
    ]


def count_edges(my_id: int, tiles: Dict[int, List[str]]) -> int:
    """Count edge matches with other tiles.

    Arguments:
        my_id (int): reference tile to match against other tiles
        tiles (Dict[int, List[str]]): tiles with their identifiers

    Returns:
        int: how many edges are matched to reference tile

    """
    count = 0
    my_normal_edges = get_edges(tiles[my_id])  # normal - not reversed
    # all edges - also reversed
    my_edges = my_normal_edges + [edge[::-1] for edge in my_normal_edges]
    for tile_id, tile in tiles.items():
        if tile_id == my_id:  # skip self
            continue
        tile_edges = get_edges(tile)
        for edge in tile_edges:
            if edge in my_edges:
                count += 1
                # break
    return count


def calc_edges(tiles: Dict[int, List[str]]) -> Dict[int, int]:
    """Find first tile that can be a corner.

    Arguments:
        tiles (Dict[int, List[str]]): tiles with their identifiers

    Returns:
        Dict[int, int]: tile_id and matched edge count

    """
    match_counts = {}
    for tile_id in tiles.keys():
        match_counts[tile_id] = count_edges(tile_id, tiles)

    return match_counts


def solution1(data: str) -> int:
    """Assemble picture and return corner tile ids multiplied

    Arguments:
        data (str): raw data, see input.txt for example

    Returns:
        int: product of assembled picture's corner tile ids

    """
    tiles = parse_tiles(data)
    counts = calc_edges(tiles)
    corner_ids = [key for key, val in counts.items() if val == 2]
    return prod(corner_ids)
