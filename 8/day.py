from typing import List, Tuple


def parse_instructions(data: str) -> List[Tuple[str, int]]:
    return [(line.split()[0], int(line.split()[1])) for line in data.splitlines()]


def solution1(data: str) -> int:
    visited_indices = []
    index = 0
    acc = 0

    instructions = parse_instructions(data)

    while index not in visited_indices:
        visited_indices.append(index)
        reg, num = instructions[index]
        if reg == "acc":
            acc += num
            index += 1
        elif reg == "nop":
            index += 1
        elif reg == "jmp":
            index += num

    return acc


def is_endless_looped(instructions: List[Tuple[str, int]]) -> bool:
    visited_indices = []
    index = 0

    while 0 <= index < len(instructions):
        if index in visited_indices:
            return True
        visited_indices.append(index)

        reg, num = instructions[index]
        if reg == "jmp":
            index += num
        else:
            index += 1

    return False


def calc_acc(instructions: List[Tuple[str, int]]) -> int:
    index = 0
    acc = 0

    while 0 <= index < len(instructions):
        reg, num = instructions[index]
        if reg == "acc":
            acc += num
            index += 1
        elif reg == "nop":
            index += 1
        elif reg == "jmp":
            index += num

    return acc


def solution2(data: str) -> int:
    corrupted_instructions = parse_instructions(data)

    if not is_endless_looped(corrupted_instructions):
        return calc_acc(corrupted_instructions)

    # go through every instruction and see if changing it helps
    for index in range(len(corrupted_instructions)):
        reg, num = corrupted_instructions[index]

        if reg == "acc":
            continue

        new_reg = "jmp" if reg == "nop" else "nop"

        new_instructions = corrupted_instructions.copy()
        new_instructions[index] = (new_reg, num)

        if not is_endless_looped(new_instructions):
            return calc_acc(new_instructions)

    raise Exception("No solutions")
