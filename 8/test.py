import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        data = """nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6"""

        expected = 5
        self.assertEqual(expected, day.solution1(data))

    def test2(self):
        data = """nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6"""

        expected = 8
        self.assertEqual(expected, day.solution2(data))