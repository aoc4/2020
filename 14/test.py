import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        data = """mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0"""

        expected = 165
        self.assertEqual(expected, day.solution1(data))

    def test_two_masks(self):
        data = """mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0
mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
mem[8] = 11
mem[7] = 101"""

        expected = 112
        self.assertEqual(expected, day.solution1(data))

    def test2(self):
        data = """mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1"""
        expected = 208
        self.assertEqual(expected, day.solution2(data))
