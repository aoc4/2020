from typing import List, Tuple
import re


def parse_assignment(line: str) -> Tuple[int, int]:
    match = re.match(r"mem\[(\d+)\] = (\d+)", line)
    return int(match.group(1)), int(match.group(2))


def parse_assignments(lines: List[str]) -> List[Tuple[int, int]]:
    return [parse_assignment(line) for line in lines]


def mask_value(value: int, mask: str) -> int:
    value_bits = "{0:b}".format(value).zfill(len(mask))
    masked_value_bits = "".join(v if m == "X" else m for v, m in zip(value_bits, mask))
    return int(masked_value_bits, 2)


def solution1(data: str) -> int:
    lines = data.splitlines()
    memory = {}
    mask = "X" * 36

    for line in lines:
        if line.startswith("mask"):
            mask = line.split(" = ")[1]
        else:
            addr, value = parse_assignment(line)
            memory[addr] = mask_value(value, mask)

    return sum(memory.values())


def get_addresses(addr: int, mask: str) -> List[int]:
    value_bits = "{0:b}".format(addr).zfill(len(mask))
    masked_bits = "".join(v if m == "0" else m for v, m in zip(value_bits, mask))
    xs = masked_bits.count("X")
    combinations = 2 ** xs
    addresses = []
    for combo in range(combinations):
        replacements = "{0:b}".format(combo).zfill(xs)
        tmp = masked_bits
        for idx in range(xs):
            tmp = tmp.replace("X", replacements[idx], 1)

        addresses.append(int(tmp, 2))
    return addresses


def solution2(data: str) -> int:
    lines = data.splitlines()
    memory = {}
    mask = "X" * 36

    for line in lines:
        if line.startswith("mask"):
            mask = line.split(" = ")[1]
        else:
            addr, value = parse_assignment(line)
            addresses = get_addresses(addr, mask)
            for address in addresses:
                memory[address] = value

    return sum(memory.values())
