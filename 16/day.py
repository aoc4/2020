from typing import Dict, List, Tuple

import re

from math import prod


def parse_rule(line: str) -> Tuple[str, List[range]]:
    match = re.match(r"(.*): (\d+)-(\d+) or (\d+)-(\d+)", line)
    name = match.group(1)
    ranges = [
        range(int(match.group(2)), int(match.group(3)) + 1),
        range(int(match.group(4)), int(match.group(5)) + 1)]
    return (name, ranges)


def parse_rules(data: str) -> Dict[str, List[range]]:
    my_ticket_idx = data.index("your ticket")
    rules = [parse_rule(rule) for rule in data[:my_ticket_idx].strip().splitlines()]
    return {name: ranges for name, ranges in rules}


def parse_my_ticket(data: str) -> List[int]:
    my_ticket_str = "your ticket"
    my_ticket_idx = data.index(my_ticket_str) + len(my_ticket_str) + 1
    nearby_ticket_idx = data.index("nearby tickets")
    my_ticket_numbers = data[my_ticket_idx: nearby_ticket_idx].strip()
    return [int(num) for num in my_ticket_numbers.split(",")]


def parse_nearby_tickets(data: str) -> List[List[int]]:
    nearby_tickets_str = "nearby tickets"
    nearby_ticket_idx = data.index(nearby_tickets_str) + len(nearby_tickets_str) + 1
    nearby_tickets = data[nearby_ticket_idx:].strip().splitlines()
    return [[int(num) for num in ticket.split(",")] for ticket in nearby_tickets]


def get_invalid_values(ticket_numbers: List[int], rules: Dict[str, List[range]]):
    invalid = []
    for num in ticket_numbers:
        valid = False
        for ranges in rules.values():
            for r in ranges:
                valid = valid or num in r
        if not valid:
            invalid.append(num)

    return invalid


def solution1(data: str) -> int:
    rules: Dict[str, List[range]] = parse_rules(data)
    nearby_tickets: List[List[int]] = parse_nearby_tickets(data)

    return sum(sum(get_invalid_values(ticket, rules)) for ticket in nearby_tickets)


def num_in_ranges(num: int, ranges: List[range]) -> bool:
    return any(num in r for r in ranges)


def get_possible_positions(rule_ranges: List[range], tickets: List[List[int]]) -> List[int]:
    # approach: use a simple and rule for a specific index
    # if it misses for a single ticket - cannot be applied
    possible_positions = [True for _ in range(len(tickets[0]))]
    for ticket in tickets:
        for pos in range(len(possible_positions)):
            possible_positions[pos] &= num_in_ranges(ticket[pos], rule_ranges)

    return [pos for pos, val in enumerate(possible_positions) if val]


def deduce_rule_indices(rules: Dict[str, List[int]]):
    sorted_rules: List[Tuple[str, List[int]]] = sorted(rules.items(), key=lambda x: len(x[1]))
    deduced = {}

    # start by smallest and pop it
    while sorted_rules != []:
        name, indices = sorted_rules.pop(0)
        # should only be 1 index
        if len(indices) != 1:
            raise Exception("Sorted rule has more than 1 possible index")

        # answer for specific rule
        deduced[name] = idx = indices[0]

        # remove assigned index from other rule possibilities
        for _, possibilities in sorted_rules:
            possibilities.remove(idx)

        sorted_rules.sort(key=lambda x: len(x[1]))


    return deduced


def solution2(data: str) -> int:
    rules: Dict[str, List[range]] = parse_rules(data)
    my_ticket: List[int] = parse_my_ticket(data)
    nearby_tickets: List[List[int]] = parse_nearby_tickets(data)
    valid_nearby_tickets = [ticket for ticket in nearby_tickets if get_invalid_values(ticket, rules) == []]

    all_tickets = [my_ticket] + valid_nearby_tickets
    possible_rule_indices: Dict[str, List[int]] = {name: get_possible_positions(ranges, all_tickets) for name, ranges in rules.items()}
    rule_indices: Dict[str, int] = deduce_rule_indices(possible_rule_indices)

    departure_rule_indices = [idx for name, idx in rule_indices.items() if name.startswith("departure")]
    my_departure_numbers = [my_ticket[idx] for idx in departure_rule_indices]
    return prod(my_departure_numbers)