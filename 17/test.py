import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        data = """.#.
..#
###"""

        expected = 3 + 5 + 3
        self.assertEqual(expected, day.solution1(data, 1))

    def test2(self):
        data = """.#.
..#
###"""

        expected = 1 + 5 + 9 + 5 + 1
        self.assertEqual(expected, day.solution1(data, 2))

    def test3(self):
        data = """.#.
..#
###"""

        expected = 5 + 10 + 8 + 10 + 5
        self.assertEqual(expected, day.solution1(data, 3))

    def test6(self):
        data = """.#.
..#
###"""

        expected = 112
        self.assertEqual(expected, day.solution1(data, 6))

    def test2_2(self):
        data = """.#.
..#
###"""
        expected = 848
        self.assertEqual(expected, day.solution2(data, 6))