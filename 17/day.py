from typing import Counter, List, Set, Tuple

import itertools


def parse_lights(data: str, dim: int = 3) -> Set[Tuple[int, int, int]]:
    lines = data.splitlines()
    lights = set()
    for y, line in enumerate(lines):
        for x in range(len(line)):
            if lines[y][x] == '#':
                lights.add(tuple([x, y] + [0] * (dim - 2)))
    return lights


def get_neighbors(light: Tuple[int, int, int], dim: int) -> List[Tuple[int, int, int]]:
    neighbors = [tuple(light[i] + v[i] for i in range(len(light))) for v in itertools.product([-1, 0, 1], repeat=len(light))]
    neighbors.remove(light)
    return neighbors


def count_active(neighbors: List[Tuple[int, int, int]], lights: Set[Tuple[int, int, int]]) -> int:
    return sum(neighbor in lights for neighbor in neighbors)


def cycle_lights(lights: Set[Tuple[int, int, int]], dim: int = 3) -> Set[Tuple[int, int, int]]:
    cycled = set()
    candidates = []

    for light in lights:
        candidates.append(light)
        neighbors = get_neighbors(light, dim)
        candidates.extend(neighbors)
        if count_active(neighbors, lights) in (2, 3):
            cycled.add(light)

    counter = Counter(candidates)

    for candidate in counter:
        if candidate not in lights:
            if counter[candidate] == 3:
                cycled.add(candidate)

    return cycled


def solution1(data: str, cycles: int) -> int:
    lights = parse_lights(data)
    for _ in range(cycles):
        lights = cycle_lights(lights)

    return len(lights)


def solution2(data: str, cycles: int) -> int:
    lights = parse_lights(data, dim=4)
    for _ in range(cycles):
        lights = cycle_lights(lights, dim=4)

    return len(lights)