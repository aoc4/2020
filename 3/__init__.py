from pathlib import Path

from . import day


if __name__ == "__main__":
    input_path = Path(__file__).parent
    with open(input_path / "input.txt") as f:
        data = f.read()

    step = (3, 1)

    print(f"solution1: {day.solution1(data, step)}")

    steps = [
        (1, 1),
        (3, 1),
        (5, 1),
        (7, 1),
        (1, 2)
    ]

    print(f"solution1: {day.solution2(data, steps)}")
