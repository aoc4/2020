from typing import Iterable, List, Tuple

from math import ceil, prod


def get_steps(map_size: Tuple[int, int], step_size: Tuple[int, int]) -> Iterable[Tuple[int, int]]:
    step_amount = ceil(map_size[1] / step_size[1])
    for step in range(step_amount):
        x = (step * step_size[0]) % (map_size[0])
        y = (step * step_size[1]) % (map_size[1])
        yield x, y


def solution1(data: str, step_size: Tuple[int, int]) -> int:
    lines: List[str] = data.splitlines()
    map_size = (len(lines[0]), len(lines))

    return sum(lines[y][x] == '#' for x, y in get_steps(map_size, step_size))


def solution2(data: str, step_sizes: List[Tuple[int, int]]) -> int:
    lines: List[str] = data.splitlines()
    map_size = (len(lines[0]), len(lines))
    sums = [sum(lines[y][x] == '#' for x, y in get_steps(map_size, step_size)) for step_size in step_sizes]
    return prod(sums)
