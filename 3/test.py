import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        data = """..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#"""
        step = (3, 1)
        expected = 7
        self.assertEqual(expected, day.solution1(data, step))

    def test2(self):
        data = """..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#"""

        steps = [
            (1, 1),
            (3, 1),
            (5, 1),
            (7, 1),
            (1, 2)
        ]

        expected = 336
        self.assertEqual(expected, day.solution2(data, steps))
