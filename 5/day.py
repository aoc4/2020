from typing import Tuple


def binary_partition(line: str, upper_char: str, upper_limit: int) -> int:
    part_range = (0, upper_limit)
    for char in line:
        upper_limit //= 2
        if char == upper_char:
            part_range = (part_range[0], part_range[1] - upper_limit)
        else:
            part_range = (part_range[0] + upper_limit, part_range[1])
    
    return int(part_range[0])  # [0] and [1] should be same

def calc_row(row: str) -> int:
    return binary_partition(row, 'F', 128)


def calc_col(col: str) -> int:
    return binary_partition(col, 'L', 8)


def parse_passport(data: str) -> Tuple[int, int, int]:
    row_partition = data[:7]
    col_partition = data[7:]
    
    row = calc_row(row_partition)
    col = calc_col(col_partition)
    seat_id = row * 8 + col
    
    return (row, col, seat_id)


def solution1(data: str) -> int:
    seat_ids = [parse_passport(line)[2] for line in data.splitlines()]
    return max(seat_ids)


def solution2(data: str) -> int:
    passports = [parse_passport(line) for line in data.splitlines()]
    seat_ids = [passport[2] for passport in passports]
    missing_seat_ids = [
        i for i in range(1024)
        if i not in seat_ids 
        and 8 < i < (1024 - 8)
    ]  # assuming that this should be just one ID, else just take first
    return missing_seat_ids[0]
