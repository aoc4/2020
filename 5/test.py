import unittest

from . import day


class DayTest(unittest.TestCase):
    def test_passport_parser1(self):
        data = "BFFFBBFRRR"

        expected = 70, 7, 567
        self.assertEqual(expected, day.parse_passport(data))

    def test_passport_parser2(self):
        data = "FFFBBBFRRR"

        expected = 14, 7, 119
        self.assertEqual(expected, day.parse_passport(data))

    def test_passport_parser3(self):
        data = "BBFFBBFRLL"

        expected = 102, 4, 820
        self.assertEqual(expected, day.parse_passport(data))

    def test_solution1(self):
        data = """BFFFBBFRRR
FFFBBBFRRR
BBFFBBFRLL"""
        expected = 820
        self.assertEqual(expected, day.solution1(data))