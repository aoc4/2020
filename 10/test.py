import unittest

from . import day


class DayTest(unittest.TestCase):
    def test1(self):
        data = """16
10
15
5
1
11
7
19
6
12
4"""

        expected = {1: 7, 2: 0, 3: 5}
        self.assertEqual(expected, day.solution1(data))

    def test2(self):
        data = """28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3"""

        expected = {1: 22, 2: 0, 3: 10}
        self.assertEqual(expected, day.solution1(data))

    def test3(self):
        data = """16
10
15
5
1
11
7
19
6
12
4"""
        expected = 8
        self.assertEqual(expected, day.solution2(data))

    def test4(self):
        data = """28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3"""

        expected = 19208
        self.assertEqual(expected, day.solution2(data))