from typing import Dict


def solution1(data: str) -> Dict[int, int]:
    # a bit lazy but: apart from adapters, socket and device values need to be added too
    adapters = [0] + sorted(int(line) for line in data.splitlines())
    adapters.append(max(adapters) + 3)

    differences = [second - first for first, second in zip(adapters, adapters[1:])]
    return {
        1: differences.count(1),
        2: differences.count(2),
        3: differences.count(3)
    }


def solution2(data: str) -> int:
    adapters = sorted(int(line) for line in data.splitlines())
    device = max(adapters)
    adapters.append(device + 3)
    # since there are loads of possible combinations
    # do a cumulative sum of possible connections from socket to device
    
    connections = {0: 1}  # starts with socket
    for adapter in adapters:
        # acceptable difference 1-3
        connections[adapter] = 0
        for diff in range(1, 3 + 1):
            connections[adapter] += connections.get(adapter - diff, 0)

    return connections[device]
